[![Release](https://img.shields.io/badge/JeroScaffold-0.1.0-blue)](https://gitee.com/ijero/jero-scaffold/releases/0.1.0)

# JeroScaffold

**scaffold-core框架已弃用LiveData，换用Flow实现数据流的发送与接收，0.0.9及之前的版本API，若已大量使用，不建议升级！！！**

## 介绍

JeroScaffold是一套基于ViewBinding+ViewModel+Flow的Android快速开发框架

## 框架介绍

- **scaffold-core：包含基类和常用扩展**
- **scaffold-ui：UI相关的扩展**

## 框架目的

1. **基类抽取**：

   本框架从开发习惯的角度出发，避免在Activity/Fragment的默认初始化方法中书写过多的初始化代码，本框架的scaffold-core包封装了基于ViewBinding的基类，在Activity基类和Fragment基类中添加了初始化相关的初始化方法，如initView、initObserver、initViewListener等，这些方法均为可选，继承基类后可按需求进行复写

2. **常用扩展**：

   - scaffold-core：包含了常用的扩展方法：如：ClipboardExt.kt（剪贴板扩展）、ContextExt.kt（上下文扩展）、DimensionExt.kt（sp/dp尺寸转换）等
   - scaffold-ui：UI相关的扩展，如EditTextExt.kt（编辑框扩展）、ToastExt.kt（toast封装扩展）、ViewExt.kt（View常用操作扩展）等

## 软件架构

- scaffold-core：快速开发的基础脚手架，由ScaffoldActivity、ScaffoldFragment、ScaffoldAdapter、ScaffoldViewModel以及一些常用的扩展库组成

- scaffold-ui：一些ui相关的的扩展库

## 安装教程

1. 项目根目录的build.gradle中添加maven仓库源

   > **注意：由于BRVAH库需要jetpack仓库源，如果要使用ScaffoldAdapter，一定要添加jitpack.io仓库源**

```groovy
allprojects {
    repositories {
        // ...
        mavenCentral()
        // scaffold-core模块中使用ScaffoldAdapter需要添加jitpack.io，不使用可以不用添加
        maven { url 'https://jitpack.io' }
        maven {
            url 'https://repo1.maven.org/maven2/'
        }
    }
}
```

2. 在主module的build.gradle中添加需要用到的依赖模块（按需添加）

```groovy
dependencies {
    // 基础脚手架
    implementation "com.gitee.ijero:scaffold-core:0.1.0"
    // ui相关的的扩展库
    implementation "com.gitee.ijero:scaffold-ui:0.1.0"
	// 如果项目使用到ScaffoldAdapter，需要添加BRVAH依赖
	implementation "com.github.CymChad:BaseRecyclerViewAdapterHelper:3.0.6"
}
```

3. 在主module的build.gradle中添加ViewBinding支持

```groovy

android {
    // ...
    buildFeatures {
        viewBinding true
    }
    // ...
}
```

## 使用说明

1. 基类用法

- ScaffoldActivity
	
```kotlin
class MainActivity : ScaffoldActivity<ActivityMainBinding>(ActivityMainBinding::inflate) {

}
```
- ScaffoldFragment

```kotlin
class DemoFragment : ScaffoldFragment<FragmentDemoBinding>(FragmentDemoBinding::inflate) {

}
```

- ScaffoldAdapter

> **使用ScaffoldAdapter前需要添加BRVAH依赖：<a href="https://github.com/CymChad/BaseRecyclerViewAdapterHelper">BaseRecyclerViewAdapterHelper</a>**

```kotlin
class DemoAdapter : ScaffoldAdapter<String, ItemDemoBinding>(ItemDemoBinding::inflate) {

}
```

2. 基类方法说明

- ScaffoldActivity：
	- initViewBefore：在初始化View之前调用，可以进行一些数据预处理
	- initView：初始化View，在[onAttachedToWindow]中调用的第一个方法，建议在此方法中进行初始化View状态，设置默认显示值等
	- initViewListener：初始化View监听器，在[initView]之后调用，建议在此方法中进行View的监听器设置
	- initObserver：初始化观察者，建议在此方法中进行观察viewModel中的数据
	- initData：初始化数据，建议在此方法中进行该界面的第一次数据加载

- ScaffoldViewModel：
	- emitError：向 error Flow发送一个错误信息，在Activity/Fragment中使用scaffoldViewModel.error.collect监听是否有错误的信息传递出来
	- emitLoading：提交一个loading信息，在Activity/Fragment中使用scaffoldViewModel.loading.collect监听数据是否在加载，在ViewModel中，加载数据前collectLoading(true)，加载完成调用collectLoading(false)，一般用于加载数据时打印日志，显示隐藏加载框等

## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

## 特别鸣谢

- [BRVAH](https://github.com/CymChad/BaseRecyclerViewAdapterHelper) ：RecyclerView的Adapter封装库
- [ViewBindingKTX](https://github.com/DylanCaiCoding/ViewBindingKTX) ：提供了ViewBinding基类封装方案思路

## 联系我

- 微信：ijerocn
- QQ：1140613720
- e-mail：<a href=mailto:ijerocn@qq.com>ijerocn@qq.com</a>

- **或者扫描二维码添加我吧：**

|微信|QQ|
|--|--|
|<img src='images/wechat_qrcode.jpg' width='260px'/>|<img src='images/qq_qrcode.jpg' width='260px'/>|
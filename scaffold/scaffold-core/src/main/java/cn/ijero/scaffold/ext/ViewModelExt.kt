package cn.ijero.scaffold.ext

import androidx.lifecycle.*
import cn.ijero.scaffold.ScaffoldViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.launchIn
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * 启动一个默认协程
 *
 * @author Jero
 */
fun <T : ScaffoldViewModel> T.launch(
    context: CoroutineContext = EmptyCoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> Unit
): Job {
    return viewModelScope.launch(context, start, block)
}

/**
 * 启动一个IO协程，与[http]
 *
 * @author Jero
 */
fun <T : ScaffoldViewModel> T.async(block: suspend CoroutineScope.() -> Unit): Job {
    return launch(Dispatchers.IO, CoroutineStart.DEFAULT, block)
}

/**
 * 启动一个IO协程，与[async]一样，只因使用习惯问题，在ViewModel中用于区分是访问网络资源
 *
 * @author Jero
 */
fun <T : ScaffoldViewModel> T.http(block: suspend CoroutineScope.() -> Unit): Job {
    return async(block)
}


/**
 * 在主线程上启动协程
 *
 * @author Jero
 */
fun <T : ScaffoldViewModel> T.main(block: suspend CoroutineScope.() -> Unit): Job {
    return launch(Dispatchers.Main, CoroutineStart.DEFAULT, block)
}

/**
 * 启动一个默认协程
 *
 * @author Jero
 */
fun <T : LifecycleOwner> T.launch(
    state: Lifecycle.State = Lifecycle.State.STARTED,
    context: CoroutineContext = EmptyCoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> Unit
): Job {
    return lifecycleScope.launch(context, start) {
        lifecycle.repeatOnLifecycle(state) {
            block()
        }
    }
}

/**
 * 在主线程上启动协程
 *
 * @author Jero
 */
fun <T : LifecycleOwner> T.main(
    state: Lifecycle.State = Lifecycle.State.STARTED ,
    block: suspend CoroutineScope.() -> Unit
): Job {
    return launch(state, Dispatchers.Main, CoroutineStart.DEFAULT, block)
}

/**
 *
 * Flow扩展的收集方法，简化在LifecycleOwner中的调用
 *
 * @author Jero
 **/
inline fun <T> Flow<T>.collect(
    lifecycleOwner: LifecycleOwner,
    state: Lifecycle.State = Lifecycle.State.STARTED,
    crossinline action: suspend (value: T) -> Unit
) {
    lifecycleOwner.launch(state) {
        collect(action)
    }
}
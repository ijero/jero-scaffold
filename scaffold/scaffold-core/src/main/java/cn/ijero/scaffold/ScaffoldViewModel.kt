package cn.ijero.scaffold

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import cn.ijero.scaffold.data.ErrorData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow

open class ScaffoldViewModel(application: Application) : AndroidViewModel(application) {
    open val context: Context
        get() = getApplication()

    private val _error = MutableSharedFlow<ErrorData>()
    val error: Flow<ErrorData> = _error

    private val _loading = MutableStateFlow(false)
    val loading: Flow<Boolean> = _loading

    /**
     *
     * 向 error liveData发送一个错误信息
     *
     * @author Jero
     **/
    open suspend fun emitError(
        error: Throwable? = null,
        msg: String? = null,
        errorCode: Int = ErrorData.DEF_ERROR_CODE
    ) {
        emitError(ErrorData(error, msg, errorCode))
    }

    /**
     *
     * 向 error liveData发送一个错误信息
     *
     * @author Jero
     **/
    open suspend fun emitError(error: ErrorData) {
        _error.emit(error)
    }

    /**
     *
     * 提交一个loading信息
     *
     * @param isLoading true 表示正在加载数据，false 表示结束加载数据
     *
     * @author Jero
     **/
    open suspend fun emitLoading(isLoading: Boolean) {
        _loading.emit(isLoading)
    }
}
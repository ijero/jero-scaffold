package cn.ijero.scaffold

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import cn.ijero.scaffold.data.ErrorData
import cn.ijero.scaffold.ext.launch
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import kotlin.random.Random

class MainViewModel(application: Application) : ScaffoldViewModel(application) {
    private val _randomInt = MutableSharedFlow<Int>()
    val randomInt: Flow<Int> = _randomInt

    fun randomInt() {
        launch {
            _randomInt.emit(Random.nextInt())
        }
    }

    fun loadError(){
        launch {
            delay(1000)
            emitError(ErrorData(IllegalStateException("模拟错误信息"),"这是一条错误信息"))
        }
    }
}
package cn.ijero.scaffold.fragment

import android.app.Application
import cn.ijero.scaffold.R
import cn.ijero.scaffold.ScaffoldViewModel
import cn.ijero.scaffold.ext.launch
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlin.random.Random

class DemoFragmentViewModel(application: Application) : ScaffoldViewModel(application) {
    private val _randomFood = MutableSharedFlow<String>()
    val randomFood: Flow<String> = _randomFood

    fun randomFood() {
        launch {
            val foods =
                context.getString(R.string.food).split(",").map { it.trim() }
            _randomFood.emit(foods[Random.nextInt(0, foods.size)])
        }
    }
}
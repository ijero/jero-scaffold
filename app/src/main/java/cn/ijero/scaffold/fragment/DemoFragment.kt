package cn.ijero.scaffold.fragment

import android.os.Bundle
import androidx.fragment.app.viewModels
import cn.ijero.scaffold.R
import cn.ijero.scaffold.ScaffoldFragment
import cn.ijero.scaffold.databinding.FragmentDemoBinding
import cn.ijero.scaffold.ext.launch
import kotlinx.coroutines.flow.collect

class DemoFragment : ScaffoldFragment<FragmentDemoBinding>(FragmentDemoBinding::inflate) {

    companion object {
        fun newInstance(): DemoFragment {
            val args = Bundle()

            val fragment = DemoFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private val viewModel by viewModels<DemoFragmentViewModel>()

    override fun initObserver() {
        launch {
            viewModel.randomFood.collect {
                binding.textView.text = getString(R.string.format_i_like_eat, it)
            }
        }
    }

    override fun initViewListener() {
        binding.serveButton.setOnClickListener {
            viewModel.randomFood()
        }
    }
}
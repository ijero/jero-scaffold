package cn.ijero.scaffold.adapter

import android.content.Context
import android.content.Intent
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import cn.ijero.scaffold.ScaffoldActivity
import cn.ijero.scaffold.databinding.ActivityDemoAdapterBinding
import cn.ijero.scaffold.ext.*
import cn.ijero.scaffold.ui.toast

class DemoAdapterActivity :
    ScaffoldActivity<ActivityDemoAdapterBinding>(ActivityDemoAdapterBinding::inflate) {

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, DemoAdapterActivity::class.java)
            context.startActivity(starter)
        }
    }

    private val viewModel by viewModels<DemoAdapterViewModel>()

    private val mAdapter by lazy {
        DemoAdapter().apply {
            setOnItemClickListener { adapter, view, position ->
                val copyText = data[position].copyText(this@DemoAdapterActivity)
                toast("成功复制内容：%s".format(copyText))
            }
        }
    }

    override fun initView() {
        super.initView()
        binding.recyclerView.adapter = mAdapter
    }

    override fun initViewListener() {
        binding.toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    override fun initObserver() {
        viewModel.demoData.collect(this)  {
            mAdapter.setList(it)
        }

        viewModel.loading.collect(this)  {
            binding.progressbar.isVisible = it
            if (it) {
                logd("正在加载数据。。。")
            } else {
                toast("数据加载完成")
                logd("加载数据完成")
            }
        }
    }

    override fun initData() {
        viewModel.loadData()
    }
}
package cn.ijero.scaffold.adapter

import cn.ijero.scaffold.databinding.ItemDemoBinding
import cn.ijero.scaffold.ScaffoldAdapter

class DemoAdapter : ScaffoldAdapter<String, ItemDemoBinding>(ItemDemoBinding::inflate) {

    override fun convert(holder: BaseBindingHolder<ItemDemoBinding>, item: String) {
        holder.binding.apply {
            textView.text = "%s".format(item)
        }
    }

}
package cn.ijero.scaffold.adapter

import android.app.Application
import cn.ijero.scaffold.ScaffoldViewModel
import cn.ijero.scaffold.data.ErrorData
import cn.ijero.scaffold.ext.async
import cn.ijero.scaffold.ext.launch
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow

class DemoAdapterViewModel(application: Application) : ScaffoldViewModel(application) {

    private val _demoData = MutableSharedFlow<MutableList<String>>()
    val demoData: Flow<MutableList<String>> = _demoData


    fun loadData() {
        launch {
            emitLoading(true)
            delay(1000)
            val data = mutableListOf(
                "哈哈哈",
                "嘿嘿嘿",
                "嘻嘻嘻",
                "快速开发脚手架",
                "关注Jero",
                "点击下面连接复制后去浏览器打开",
                "https://gitee.com/ijero/jero-scaffold",
                "蟹蟹支持，么么哒~~"
            )
            _demoData.emit(data)
            emitLoading(false)
        }
    }

}
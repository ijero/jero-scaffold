package cn.ijero.scaffold

import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Lifecycle
import cn.ijero.scaffold.adapter.DemoAdapterActivity
import cn.ijero.scaffold.databinding.ActivityMainBinding
import cn.ijero.scaffold.ext.collect
import cn.ijero.scaffold.ext.launch
import cn.ijero.scaffold.ext.loge
import cn.ijero.scaffold.ext.main
import cn.ijero.scaffold.fragment.FragmentDemoActivity
import cn.ijero.scaffold.ui.toast
import kotlinx.coroutines.flow.collect

class MainActivity : ScaffoldActivity<ActivityMainBinding>(ActivityMainBinding::inflate) {

    private val viewModel by viewModels<MainViewModel>()

    override fun initView() {
        binding.textView.text = "JeroScaffold1"
    }

    override fun initObserver() {
        viewModel.randomInt.collect(this) {
            binding.textView.text = "$it"
        }

        viewModel.error.collect(this) {
            loge(it.error, it.msg)
            toast(it)
        }
    }

    fun random(view: View) {
        viewModel.randomInt()
    }

    fun error(view: View) {
        viewModel.loadError()
    }

    fun toFragment(view: View) {
        FragmentDemoActivity.start(this)
    }

    fun toAdapter(view: View) {
        DemoAdapterActivity.start(this)
    }

}